import {
  gql
} from 'apollo-boost';

export const AUTHORS_QUERY = gql `
{
  authors {
    id
    name
  }
}
`

export const BOOKS_QUERY = gql `
  {
    books {
      id
      name
      genre
      author {
        name
        age
      }
    }
  }
`