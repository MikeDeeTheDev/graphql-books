import React, { Component } from 'react';
import { useQuery, useMutation } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { AUTHORS_QUERY, BOOKS_QUERY } from './queries';
import { ADD_BOOK } from './mutations'


function AddBook() {
    let title, genre, author;
    const { loading, error, data } = useQuery(AUTHORS_QUERY);
    const [addBook, { data: mutatedData }, ] = useMutation(ADD_BOOK, {
        refetchQueries: [{ query: BOOKS_QUERY }],
    });

    if (loading) return <p>Loading...</p>;
    if (error) return <p>{`Error :(  ${error}`}</p>;

    return (
        <form id="add-book" onSubmit={e => {
            e.preventDefault();
            addBook({ variables: {
                name: title.value,
                genre: genre.value,
                authorId: author.value
            } });
            title.value = '';
            genre.value = '';
        }}>
            <div className="field">
                <label>Book Title:</label>
                <input type="text" ref={node => {
                        title = node;
                    }}
                />
            </div>

            <div className="field">
                <label>Genre:</label>
                <input type="text" ref={node => {
                    genre = node;
                }} />
            </div>

            <div className="field">
                <label>Author:</label>
                <select ref={node => {
                    author = node;
                }}>
                    <option>Select Author</option>
                    { data.authors.map(({ id, name }) => (
                        <option value={id}>{`${name}`}</option>
                    ))}
                </select>
            </div>

            <button type="submit">+</button>
        </form>
    )
}

export default AddBook;