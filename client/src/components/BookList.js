import React, { Component } from 'react';
import { useQuery } from '@apollo/react-hooks';
import { gql } from 'apollo-boost';
import { BOOKS_QUERY } from './queries';


function BookList() {
    const { loading, error, data } = useQuery(BOOKS_QUERY);

    if (loading) return <p>Loading...</p>;
    if (error) return <p>{`Error :( ${error}`}</p>
    return (
        <div id="main">
            <ul id="book-list">
                { data.books.map(({ id, name, genre, author: { name: authorName, age } }) => (
                    <li key={id}>
                        {`${name} (${genre}), written by ${authorName} (Age ${age})`}
                    </li>
                ))}
            </ul>
        </div>
    );
}

export default BookList;
