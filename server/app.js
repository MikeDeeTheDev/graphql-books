const express = require('express');
const ExpressGraphQL = require('express-graphql');
const schema = require('./schema/schema');
const mongoose = require('mongoose');
const cors = require('cors');

const app = express();

mongoose.connect('mongodb+srv://mikedee:Test1234@cluster0-dadja.mongodb.net/test?retryWrites=true&w=majority');
mongoose.connection.once('open', () => console.log('Connected to Database'));

app.use(cors());

app.use('/graphql', ExpressGraphQL({
    schema: schema,
    graphiql: true,
}));

app.listen(4000, () => console.log('Server listening on port 4000'));