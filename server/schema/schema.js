const _ = require('lodash');
const Book = require('../models/book');
const Author = require('../models/author');

const {
    GraphQLSchema,
    GraphQLObjectType,
    GraphQLString,
    GraphQLInt,
    GraphQLList,
    GraphQLID,
    GraphQLNonNull,
} = require('graphql');

const BookType = new GraphQLObjectType({
    name: 'Book',
    description: 'This represents a book',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLNonNull(GraphQLString) },
        authorId: { type: GraphQLNonNull(GraphQLID) },
        genre: { type: GraphQLNonNull(GraphQLString) },
        author: {
            type: AuthorType,
            resolve: (book) => Author.findById(book.authorId)
        }
    })
});

const AuthorType = new GraphQLObjectType({
    name: 'Author',
    description: 'This represents an author',
    fields: () => ({
        id: { type: GraphQLID },
        name: { type: GraphQLNonNull(GraphQLString) },
        age: { type: GraphQLNonNull(GraphQLInt) },
        books: {
            type: GraphQLList(BookType),
            resolve: (author) => Book.find({ authorId: author.id })
        }
    })
});

const RootQuery = new GraphQLObjectType({
    name: 'Query',
    description: 'The root query',
    fields: {
        book: {
            type: BookType,
            args: {
                id: { type: GraphQLID }
            },
            resolve: (parent, args) => Book.findById(args.id)
        },
        books: {
            type: GraphQLList(BookType),
            resolve: () => Book.find()
        },
        author: {
            type: AuthorType,
            args: {
                id: { type: GraphQLID }
            },
            resolve: (parent, args) => Author.findById(args.id)
        },
        authors: {
            type: GraphQLList(AuthorType),
            resolve: () => Author.find()
        }
    }
});

const RootMutation = new GraphQLObjectType({
    name: 'Mutation',
    description: 'The root mutation',
    fields: {
        addBook: {
            type: BookType,
            args: {
                name: { type: GraphQLNonNull(GraphQLString) },
                genre: { type: GraphQLNonNull(GraphQLString) },
                authorId: { type: GraphQLNonNull(GraphQLID) },
            },
            resolve: (parent, args) => {
                const book = new Book({...args});
                return book.save();
            }
        },
        addAuthor: {
            type: AuthorType,
            args: {
                name: { type: GraphQLNonNull(GraphQLString) },
                age: { type: GraphQLNonNull(GraphQLInt) },
            },
            resolve: (parent, args) => {
                const author = new Author({...args});
                return author.save();
            }
        }
    }
});

module.exports = new GraphQLSchema({
    query: RootQuery,
    mutation: RootMutation,
});